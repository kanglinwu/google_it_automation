from PIL import Image

im = Image.open("1.jpg")

# resize
new_im = im.resize((640,480))
new_im.save("resize_1.jpg")

# rotate
new_im_2 = im.rotate(90)
new_im_2.save("rotate_1.jpg")

#! /usr/bin/env python3

from rearrange import rearrange_name

import unittest

class TestRearrange(unittest.TestCase):
    def test_basic(self):
        testcase = "Lovelace, Ada"
        expected = "Ada Lovelace"
        # the assertEqual method basically says both of my arguments are equal. 
        # If that statement's true, then the test passes.
        # If it's false, the test fails and error is printed to the screen when the test is run. 
        self.assertEqual(rearrange_name(testcase), expected) 

    def test_empty(self):
        # Edge case
        testcase = ""
        expected = ""
        self.assertEqual(rearrange_name(testcase), expected) 

    def test_double_name(self):
        testcase = "Hopper, Grace M."
        expected = "Grace M. Hopper"
        self.assertEqual(rearrange_name(testcase), expected) 

    def test_one_name(self):
        testcase = "Voltaire"
        expected = "Voltaire"
        self.assertEqual(rearrange_name(testcase), expected) 

# how to run the test?
# In the main part of our program, we'll call the unittest.main() function, which will run the test for us.
unittest.main()